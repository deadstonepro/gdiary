import os
import datetime
import requests
import pytz
from telegram import Update
from telegram.ext import Updater, CallbackContext, MessageHandler, Filters


def _to_kiev_ts(dt: datetime.datetime) -> float:
    kiev_tz = pytz.timezone("Europe/Kiev")
    return dt.astimezone(kiev_tz).timestamp()


class DairyHandler:

    def __init__(self):
        self.user = os.environ["USER"]
        self.token = os.environ["TOKEN"]
        self.submit = os.environ["SUBMIT"]

    def on_message(self, update: Update, context: CallbackContext):
        if update.message.from_user.username != self.user:
            update.message.reply_text("You are not allowed to user this bot.")
            return
        data = {
            "date": _to_kiev_ts(update.message.date),
            "text": update.message.text
        }
        requests.post(self.submit, json=data)


if __name__ == "__main__":
    dairy_handler = DairyHandler()
    updater = Updater(dairy_handler.token)
    updater.dispatcher.add_handler(MessageHandler(Filters.all,
                                   dairy_handler.on_message))
    updater.start_polling()
    updater.idle()
