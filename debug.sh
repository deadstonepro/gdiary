#!/bin/bash

trap cleanup EXIT
source ./secrets.sh

redis-server 2>&1 > /dev/null & 
PID1=$!
echo "**** Redis server started ****"

export TEMPLATES_AUTO_RELOAD=True
export TEMPLATES_AUTO_RELOAD=True
export FLASK_ENV=development
export REDIS_URL=localhost
python3 server/src/web.py 2>&1 &
PID2=$!
echo "**** Bot started. Check logs in STDIN ****"

# export USER=...
# export TOKEN=...
export SUBMIT=http://localhost:5000/submit
python3 bot/bot.py >&2 &
PID3=$!
echo "**** Web started. Check logs in STDERR ****"

cleanup() {
    echo "**** Exiting ****"
    echo "Killing $PID1 $PID2 $PID3"
    kill -9 $PID1
    kill -9 $PID2
    kill -9 $PID3
}

wait %1 %2 %3
