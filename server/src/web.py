import os
import datetime
import dataclasses
from typing import List
import pytz
import redis
from flask import Flask, request, render_template, send_from_directory


class DairyDB:
    @dataclasses.dataclass
    class Post:
        date: float  # unix timestamp
        text: str

    def __init__(self, app: Flask):
        assert app.config["REDIS_URL"]
        self._conn = redis.Redis(app.config["REDIS_URL"])
        setattr(app, "db", self)

    def push_post(self, post: Post):
        post_ids = self._conn.lrange("posts", 0, -1)
        if not post_ids:
            next_id = 0
        else:
            last_id = int(post_ids[-1])
            next_id = last_id + 1
        post_key = "post:{}".format(next_id)
        for key, value in dataclasses.asdict(post).items():
            self._conn.hset(post_key, key, value)
        self._conn.rpush("posts", next_id)

    def get_posts(self) -> List[Post]:
        result = []
        post_ids = map(int, self._conn.lrange("posts", 0, -1))
        for post_id in post_ids:
            post_key = "post:{}".format(post_id)
            fields = dict((k.decode(), v.decode())
                          for (k, v) in self._conn.hgetall(post_key).items())
            result.append(DairyDB.Post(**fields))
        return result

    def todays_posts(self) -> List[Post]:
        today = datetime.datetime.now()
        days_begin_ts = datetime.datetime(year=today.year, month=today.month,
                                          day=today.day).timestamp()
        return [p for p in self.get_posts() if float(p.date) >= days_begin_ts]


app = Flask(__name__)
app.config["REDIS_URL"] = os.environ['REDIS_URL']
db = DairyDB(app)


@app.route("/submit", methods=["POST"])
def submit_route():
    app.db.push_post(app.db.Post(**request.json))
    return "Everythink was good.", 200


@app.route("/", methods=["GET"])
def index_route():
    def ts_fmt(ts) -> str:
        ts = float(ts)
        dt = datetime.datetime.fromtimestamp(ts)
        dt = dt.astimezone(pytz.timezone("Europe/Kiev"))
        return dt.strftime('%H:%M')

    def is_near(p1: DairyDB.Post, p2: DairyDB.Post) -> bool:
        dt1 = datetime.datetime.fromtimestamp(float(p1.date))
        dt2 = datetime.datetime.fromtimestamp(float(p2.date))
        return dt2 - dt1 <= datetime.timedelta(minutes=10)

    return render_template("index.html.j2", posts=app.db.todays_posts(),
                           ts_fmt=ts_fmt, is_near=is_near)


@app.route("/static/<fname>", methods=["GET"])
def static_route(fname):
    return send_from_directory("./static/", fname)


if __name__ == "__main__":
    app.run(host="localhost", port=5000)
