## George's Diary

### Deply
* Clone the project to the desination machine.
* Install docker and docker-compose (>=3.8).
* Replace all '...' in the docker-compose.yml with your values: 
    * `TOKEN` – Token of the telegram bot. 
    * `USER` – Username of the only user who allowed to use the bot (w/o `@`).
* Change the port (that is exposed by uwsgi, by default is :5000).
* Change the mountpoint of the db/. (Optional).
* `docker-compose up -d`
* Proxy it by nginx (Optional, but highly recommended).

### Debugging
See `debug.sh`. Written with love.
